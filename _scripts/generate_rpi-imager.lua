local lyaml = require("lyaml")
local rapidjson = require("rapidjson")

local function read_yaml_file(file_path)
	local file = assert(io.open(file_path, "r"))
	local content = file:read("*all")
	file:close()
	return lyaml.load(content)
end

local function ends_with(str, ending)
	return str:sub(-#ending) == ending
end

local arch = {
	armhf = {
		devices = { "pi1-32bit" },
		bits = "32bit",
		desc = "Small OS for RPi 1 and Zero/W",
	},
	armv7 = {
		devices = { "pi2-32bit", "pi3-32bit" },
		bits = "32bit",
		desc = "Small OS for RPi 2 and 3",
	},
	aarch64 = {
		devices = { "pi3-64bit", "pi4-64bit", "pi5-64bit" },
		bits = "64bit",
		desc = "Small OS for RPi 3, 4 and 5",
	},
}

local function process_yaml_file(file_path, os_list)
	local yaml_data = read_yaml_file(file_path)

	for _, entry in ipairs(yaml_data) do
		-- Filter based on the flavor being "alpine-rpi" and file ending with "img.gz"
		if entry.flavor == "alpine-rpi" and ends_with(entry.file, "img.gz") then
			local os_entry = {
				name = "Alpine Linux " .. entry.version .. " (" .. arch[entry.arch].bits .. ")",
				description = arch[entry.arch].desc,
				url = "https://dl-cdn.alpinelinux.org/alpine/"
					.. entry.branch
					.. "/releases/"
					.. entry.arch
					.. "/"
					.. entry.file,
				icon = "https://alpinelinux.org/alpinelinux-logo-icon.svg",
				website = "https://alpinelinux.org/",
				release_date = entry.date,
				extract_size = entry.extracted_size,
				extract_sha256 = entry.extracted_sha256,
				image_download_size = entry.size,
				image_download_sha256 = entry.sha256,
				devices = arch[entry.arch].devices,
				init_format = "none",
			}
			table.insert(os_list, os_entry)
		end
	end
end

if #arg < 1 then
	error("Usage: lua script.lua <yaml_file1> <yaml_file2> ...")
end

local os_list = {}

for i = 1, #arg do
	local file_path = arg[i]
	process_yaml_file(file_path, os_list)
end

print(rapidjson.encode({ os_list = os_list }))
