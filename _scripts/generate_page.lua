#!/usr/bin/lua

local markdown = require("discount")
local lyaml = require("lyaml")
local lustache = require("lustache")

local function read_markdown(file)
	local f = assert(io.open(file))
	local data = f:read("*a")
	f:close()
	local yml, md = data:match("^(%-%-%-.-%-%-%-)(.*)$")
	local pagename = file:gsub(".md$", "")
	local pageclass = pagename:gsub("/.*", "")
	if yml and md then
		local t = lyaml.load(yml)
		t.pagename = pagename
		t.pageclass = pageclass
		return t, markdown(md)
	else
		return { title = pagename:gsub("/index$", ""), pagename = pagename, pageclass = pageclass }, markdown(data)
	end
end

local function read_layout(file)
	-- try look for template for 'path/file.md' in this order:
	--   path/file.template.html
	--   layout.template.html
	for _, t in pairs({ file:gsub(".md$", ".template.html"), "_default.template.html" }) do
		local f = io.open(t)
		if f then
			local data = f:read("*a")
			f:close()
			return data
		end
	end
end

local function import_yaml(filename)
	local f = assert(io.open(filename))
	local t = lyaml.load(f:read("*a*"))
	f:close()
	return t
end

local page, content = read_markdown(assert(arg[1]))
local layout = read_layout(arg[1])
for i = 2, #arg do
	local t = {}
	for k, v in pairs(import_yaml(arg[i])) do
		t[k] = v
	end
	local tname = string.gsub(arg[i], ".yaml$", "")
	page[tname] = t
end

page.pagestate = {}
page.pagestate[page.pagename] = "active"
page.current_year = os.date("%Y")

page.content = lustache:render(content, page)

if page.date then
	local y, m, d = page.date:match("(%d+)-(%d+)-(%d+)")
	page.pubdate = os.date("%b %d, %Y", os.time({ year = y, month = m, day = d }))
end

io.write(lustache:render(layout, page))
