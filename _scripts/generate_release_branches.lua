#!/usr/bin/lua

local lyaml = require("lyaml")

-- color to inidicate the support status
local function color_str(n)
	if n < 4 then
		return "green"
	elseif n < 5 then
		return "orange"
	end
	return "red"
end

local function support_level(n)
	if n < 2 then
		return "main and community"
	elseif n < 5 then
		return "main"
	elseif n < 11 then
		return "on request"
	end
	return "none"
end

local t = {}
for i = 1, #arg do
	local f = assert(io.open(arg[i]))
	local n = 0

	for _, v in pairs(lyaml.load(f:read("*a"))) do
		if v.eol_date ~= nil then
			local y, m, d = v.eol_date:match("(%d+)-(%d+)-(%d+)")
			v.eol_datestr = os.date("%B, %Y", os.time({ year = y, month = m, day = d }))
		end
		v.eol_color = color_str(n)
		v.support_level = support_level(n)
		n = n + 1
		if v.releases then
			local rels = {}
			for _, r in pairs(v.releases) do
				r.url = string.gsub(r.notes or "", "(.*).md$", "../%1.html")
				table.insert(rels, r)
			end
			v.releases = rels
			v.latest = v.releases[1]
		end

		table.insert(t, v)
	end
	f:close()
end

io.write(lyaml.dump({ t }))
