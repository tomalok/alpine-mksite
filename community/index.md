---
title: Alpine Linux Community
---

# Alpine Linux Community

Participation in the Alpine community requires adherence to the [Code of Conduct](code-of-conduct.html).

<h2><i class="fa fa-hashtag" aria-hidden="true"></i>IRC (irc.oftc.net)</h2>

* [\#alpine-linux](irc://irc.oftc.net/alpine-linux) - For general discussion and quick support questions.
* [\#alpine-devel](irc://irc.oftc.net/alpine-devel) - For discussion of Alpine Linux development and developer
  support.
* [\#alpine-offtopic](irc://irc.oftc.net/alpine-offtopic) - For off-topic discussions with Alpine community members.

The IRC channels are also accessible through the Matrix IRC bridge at `#_oftc_#channame:matrix.org` -
for example, `#_oftc_#alpine-linux:matrix.org`.
However, as Matrix messages are being "simplified" for IRC,
replies, edits, or reactions should not be used to avoid confusion or spam.

<h2><i class="fa fa-envelope" aria-hidden="true"></i>Mailing Lists</h2>

Mailing lists for development, support, etc, can be found here:

[https://lists.alpinelinux.org/lists/~alpine](https://lists.alpinelinux.org/lists/~alpine)

<h2><i class="fa fa-pencil" aria-hidden="true"></i>Wiki</h2>

Wiki has the documentation.

<https://wiki.alpinelinux.org>

<h2><i class="fa-brands fa-mastodon" aria-hidden="true"></i>Mastodon</h2>

<a href="https://fosstodon.org/@alpinelinux">@alpinelinux@fosstodon.org</a>

<h2><i class="fa-brands fa-x-twitter" aria-hidden="true"></i>X.com</h2>

<https://x.com/alpinelinux>

