<!-- begin cloud -->
<script type="text/javascript" src="/js/cloud.js"></script>
<div class="pure-g">

    <div class="pure-u-1">
        <div class="l-box">
            <h1>Cloud Images</h1>
            <p> <i>These images are built using the <a
                href="https://gitlab.alpinelinux.org/alpine/cloud/alpine-cloud-images">
                Alpine Linux Cloud Images Builder</a>.<br/>
                To report problems or request features, please open an <a
                href="https://gitlab.alpinelinux.org/alpine/cloud/alpine-cloud-images/-/issues">
                issue</a> with <u>detailed</u> information.</i>
            </p>
            <h3><b>NOTES</b></h3>
            <ul>
            <li>Image variants are built for all cloud-valid combinations of
                CPU <b>architecture</b> <i>(aarch64 or x86_64)</i>,
                <b>firmware</b> <i>(BIOS or UEFI)</i>,
                instance <b>bootstrap</b> system <i>(<a
                    href="https://gitlab.alpinelinux.org/alpine/cloud/tiny-cloud"
                    >Tiny Cloud</a> or <a href="https://cloud-init.io">cloud-init)</a></i>,
                and <b>machine type</b> <i>(virtual or bare-metal)</i>.
                <br/><br/></li>
            <li>The login user for all images is "<b><kbd>alpine</kbd></b>". SSH keys for this
                user will be installed from the cloud's instance metadata service (IMDS).
                <br/><br/></li>
            <li> Please refer to <a
                href="https://gitlab.alpinelinux.org/alpine/cloud/alpine-cloud-images/-/blob/main/IMPORTING.md"
                >this <i>(work-in-progress)</i> document</a> for some (rough) guidance
                on how to import and launch these images with various cloud providers.
                <br/><br/></li>
            <li><b>AWS</b> images are well-tested and directly launchable from this page
                (select a "Launch Region" to show region links); they are also
                downloadable for importing into your own cloud accounts.
                <br/><br/></li>
            <li><b>Azure</b>, <b>GCP</b>, <b>OCI</b>, and <b>NoCloud</b> images are considered
                <i><u>beta</u></i> quality; and have been used successfully.  <i>Cloud-specific
                images will eventually be replaced by generic images.</i>
                <br/><br/></li>
            <li><b>Generic</b> images are considered <i><u>alpha</u></i> quality.  However, they represent
                the intended future direction of this project, and <i>should</i> work with
                any cloud provider that <a
                href="https://gitlab.alpinelinux.org/alpine/cloud/tiny-cloud">Tiny Cloud</a> or
                <a href="https://cloud-init.io">cloud-init</a> are able to auto-detect, but are
                not well-tested at this time.
                <br/><br/></li>
            <li>Images are signed with this GPG public key <i class="fa fa-lock"></i>&nbsp;<a
                href="/keys/tomalok.asc">F26A DFAD BAE7 02EF 7AF6 3745 9DA7 EF23 BFFC DF22</a>
                <br/><br/></li>
            </ul>
            <hr/>
            <form class="pure-form pure-form-stacked">
                <fieldset>
                    <div class="pure-g">
                        <div class="pure-u-1 pure-u-md-1-3">
                            <label for="cloud"><b>Cloud Provider</b></label>
                            <select id="f_cloud" class="filter" onchange="filter(this.id)">
                                <option value="*">(all)</option>
                                {{#cloud/releases.filters.clouds}}
                                <option value="{{cloud}}">{{cloud_name}}</option>
                                {{/cloud/releases.filters.clouds}}
                            </select>
                        </div>
                        <div class="pure-u-1 pure-u-md-1-6 f_cloud v_aws">
                            <label for="region"><b>Launch Region</b></label>
                            <select id="f_region" class="filter" onchange="filter(this.id)">
                                <option value="">(select)</option>
                                {{#cloud/releases.filters.regions}}
                                <option value="{{region}}" id="f_{{region}}"
                                    class="f_cloud {{#clouds}}v_{{cloud}}{{/clouds}}">{{region}}</option>
                                {{/cloud/releases.filters.regions}}
                            </select>
                        </div>
                    </div>
                    <div class="pure-g">
                        <div class="pure-u-1 pure-u-md-1-8">
                            <label for="release"><b>Release</b></label>
                            <select id="f_release" class="filter" onchange="filter(this.id)">
                                <option value="*">(all)</option>
                                {{#cloud/releases.versions}}
                                <option value="{{release}}">{{release}}</option>
                                {{/cloud/releases.versions}}
                            </select>
                        </div>
                        <div class="pure-u-1 pure-u-md-1-8">
                            <label for="arch"><b>Arch</b></label>
                            <select id="f_arch" class="filter" onchange="filter(this.id)">
                                <option value="*">(all)</option>
                                {{#cloud/releases.filters.archs}}
                                <option value="{{arch}}">{{arch_name}}</option>
                                {{/cloud/releases.filters.archs}}
                            </select>
                        </div>
                        <div class="pure-u-1 pure-u-md-1-8">
                            <label for="firmware"><b>Firmware</b></label>
                            <select id="f_firmware" class="filter" onchange="filter(this.id)">
                                <option value="*">(all)</option>
                                {{#cloud/releases.filters.firmwares}}
                                <option value="{{firmware}}">{{firmware_name}}</option>
                                {{/cloud/releases.filters.firmwares}}
                            </select>
                        </div>
                        <div class="pure-u-1 pure-u-md-1-8">
                            <label for="bootstrap"><b>Bootstrap</b></label>
                            <select id="f_bootstrap" class="filter" onchange="filter(this.id)">
                                <option value="*">(all)</option>
                                {{#cloud/releases.filters.bootstraps}}
                                <option value="{{bootstrap}}">{{bootstrap_name}}</option>
                                {{/cloud/releases.filters.bootstraps}}
                            </select>
                        </div>
                        <div class="pure-u-1 pure-u-md-1-8">
                            <label for="machine"><b>Machine</b></label>
                            <select id="f_machine" class="filter" onchange="filter(this.id)">
                                <option value="*">(all)</option>
                                {{#cloud/releases.filters.machines}}
                                <option value="{{machine}}">{{machine_name}}</option>
                                {{/cloud/releases.filters.machines}}
                            </select>
                        </div>
                    </div>
                </fieldset>
            </form>
            <br/>
        </div>
    </div>

    {{#cloud/releases.versions}}
    <div id="f_{{release}}" class="pure-u-1 pure-u-md-1-2 f_release v_{{release}}">
        <div class="cloud download">
            <h2 title="End of Life: {{end_of_life}}">{{release}}</h2>
            {{#images}}
            <div id="f_{{variant}}" class="f_arch v_{{arch}} f_firmware v_{{firmware}} f_bootstrap v_{{bootstrap}} f_machine v_{{machine}}">
                <table class="pure-table pure-table-bordered">
                    <thead>
                        <tr><td colspan="3" align="center">
                            <b title="Released: {{released}}"">{{arch}} • {{firmware}} • {{bootstrap}} • {{machine}}</b>
                            <table class="pure-table pure-table-horizontal" style="border-top: 1.5px solid #999;">
                                {{#downloads}}
                                <tr id="f_{{variant}}_{{cloud}}" class="f_cloud v_{{cloud}}">
                                    <td align="center"><a href="{{{image_url}}}.{{{image_format}}}" class="green-button"
                                        title="{{image_name}}.{{image_format}}">
                                        <i class="fa fa-download"></i>&nbsp;{{cloud}}
                                    </a></td>
                                    <td align="center">
                                        <a title="GPG signature"
                                           class="pure-button checksums"
                                           href="{{{image_url}}}.asc"
                                           ><i class="fa fa-lock"></i>&nbsp;gpg</a>
                                        <a title="YAML metadata"
                                           class="pure-button checksums"
                                           href="{{{image_url}}}.yaml"
                                           ><i class="fa fa-info-circle"></i>&nbsp;meta</a>
                                        {{#regions}}
                                        <a id="f_{{variant}}_{{cloud}}_{{region}}_l"
                                            class="pure-button launch f_cloud v_{{cloud}} f_region v_{{region}}"
                                            title="launch in {{cloud}} / {{region}}"
                                            target="_blank" rel="noopener noreferrer"
                                            href="{{{launch_url}}}" style="display: none;"
                                            ><i class="fa fa-rocket"></i>&nbsp;launch</a>
                                        <a id="f_{{variant}}_{{cloud}}_{{region}}_i"
                                            class="pure-button launch f_cloud v_{{cloud}} f_region v_{{region}}"
                                            title="image in {{cloud}} / {{region}}"
                                            target="_blank" rel="noopener noreferrer"
                                            href="{{{region_url}}}" style="display: none;"
                                            ><i class="fa fa-info-circle"></i>&nbsp;image</a>
                                        {{/regions}}
                                    </td>
                                </tr>
                                {{/downloads}}
                            </table>
                        </td></tr>
                    </thead>
                </table>
            </div>
            {{/images}}
        </div>
    </div>
    {{/cloud/releases.versions}}

</div>
<!-- end cloud -->
