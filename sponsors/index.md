# Sponsors

<div class="pure-g">
 <div class="pure-u-1 pure-u-md-1-2">
  <a href="https://www.gigabyte.com/"><img class="logo" alt="GIGABYTE Logo" src="/gigabyte-small.png"/></a>
 </div>
 <div class="pure-u-1 pure-u-md-1-2">
  <a href="https://vpsfree.org"><img class="logo" alt="vpsFree Logo" src="/vpsfree-small.png"/></a>
 </div>
 <div class="pure-u-1 pure-u-md-1-2">
  <a href="https://www.linode.com/"><img class="logo" alt="Linode Logo" src="/linode-small.png"/></a>
 </div>
 <div class="pure-u-1 pure-u-md-1-2">
  <a href="https://fastly.com"><img class="logo" alt="Fastly Logo" src="/logo_fastly.png"/></a>
 </div>
 <div class="pure-u-1 pure-u-md-1-2">
  <a href="https://metal.equinix.com"><img class="logo" alt="Equinix Metal Logo" src="/equinix-metal-small.png"/></a>
 </div>
 <div class="pure-u-1 pure-u-md-1-2">
  <a href="https://alpinelinuxsupport.com"><img class="logo" alt="AlpineLinuxSupport Logo" src="/logo_alpinelinuxsupport3-small.png"/></a>
 </div>
</div>
