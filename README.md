# Alpine-mksite

Website builder for alpinelinux.org.

## Local dev

The following dependencies are needed to build the site:

~~~bash
apk add \
    busybox-extras \
    curl \
    git \
    lua5.3 \
    lua-rapidjson \
    lua-discount \
    lua-feedparser \
    lua-filesystem \
    lua-lustache \
    lua-lyaml
~~~

To build the site please run:

~~~bash
make
~~~

To set up a local web server please run:

~~~bash
busybox-extras httpd -c ../httpd.conf -p 8000 -h _out/
~~~

## Docker use

~~~bash
# build
docker build -t alpine-mksite .
# run
docker run -p 8000:8000 alpine-mksite
# run on different port without exposing it
docker run alpine-mksite --port 9090
~~~

## Accessing the service

Point your browser to: http://localhost:8000
