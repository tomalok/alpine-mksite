---
title: Alpine Conference Spring 2021
---

# AlpineConf 2021

We are pleased to present AlpineConf 2021, the first Alpine Linux conference.
The conference is being held online May 15th and 16th. The conference will
consist of talks from the community about Alpine Linux and the ecosystem around
it. There will also be discussions, demos, and an announcement at the end.

* [Watch online](https://bbb.dereferenced.org/b/adm-ec4-bx7-ypm)
* [Offtopic stream](https://bbb.dereferenced.org/b/ari-s7n-qby-nbp)

## Saturday May 15th

<div class="conf-schedule-day">
    <table class="schedule">
        <tr>
            <td>Time (<abbr title="Central European Summer Time (UTC+2)">CEST</abbr>)</td>
            <td>Event</td>
        </tr>
        {{#conf/schedule.day1}}
        <tr>
            <td class="time">{{time}}</td>
            <td class="event {{type}}">{{event}}</td>
        </tr>
        {{/conf/schedule.day1}}
    </table>
    <table class="key">
        <tr>
            <td class="event long">50-minute talk</td>
        </tr>
        <tr>
            <td class="event short">25-minute talk</td>
        </tr>
        <tr>
            <td class="event other">Other</td>
        </tr>
    </table>
</div>


## Sunday May 16th


<div class="conf-schedule-day">
    <table class="schedule">
        <tr>
            <td>Time (<abbr title="Central European Summer Time (UTC+2)">CEST</abbr>)</td>
            <td>Event</td>
        </tr>
        {{#conf/schedule.day2}}
        <tr>
            <td class="time">{{time}}</td>
            <td class="event {{type}}">{{event}}</td>
        </tr>
        {{/conf/schedule.day2}}
    </table>
    <table class="key">
        <tr>
            <td class="event long">50-minute talk</td>
        </tr>
        <tr>
            <td class="event short">25-minute talk</td>
        </tr>
        <tr>
            <td class="event other">Other</td>
        </tr>
    </table>
</div>
