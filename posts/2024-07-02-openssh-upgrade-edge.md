---
title: 'Restart required for sshd after upgrade to 9.8_p1 on edge'
date: 2024-07-02
---

Restart required for sshd after update to 9.8_p1 on edge
===========================

Due the changes in openssh, after upgrading openssh to 9.8_p1 or higher, a
restart of sshd is required. Otherwise sshd is not able to accept new
connections.

This only affects Alpine Linux edge, and will affect the upcoming 3.21 release.

After upgrading, run the following command:

    rc-service sshd restart

From the [OpenSSH 9.8_p1 release notes][0]:

>  * sshd(8): the server has been split into a listener binary, sshd(8),
   and a per-session binary "sshd-session". This allows for a much
   smaller listener binary, as it no longer needs to support the SSH
   protocol. As part of this work, support for disabling privilege
   separation (which previously required code changes to disable) and
   disabling re-execution of sshd(8) has been removed. Further
   separation of sshd-session into additional, minimal binaries is
   planned for the future.

[0]:https://www.openssh.com/releasenotes.html#9.8p1
