---
title: 'Seeking Support After Equinix Metal Sunsets'
date: 2025-02-03
---
Seeking Support After Equinix Metal Sunsets
===========================================

We are deeply grateful to [Equinix Metal](https://deploy.equinix.com) (formerly
Packet.net) for their longstanding support, which has been essential to Alpine
Linux's ecosystem. However, with Equinix sunsetting their bare-metal hosting
service, this critical support will soon end. Their contributions have been
invaluable, and we thank them for helping keep Alpine Linux reliable and
efficient.

This change poses a significant challenge, as it impacts key infrastructure
hosted with Equinix. To ensure continuity, we must now find alternative hosting
solutions. Below, we outline the affected infrastructure and how the community
can help us navigate this transition.

## Infrastructure Currently Hosted with Equinix Metal

1. *Storage Services for T1 Mirroring Infra*
   Equinix has been hosting three storage services that power our T1
   mirroring infrastructure. These servers are the backbone of
   [dl-cdn.alpinelinux.org](https://dl-cdn.alpinelinux.org), enabling fast and
   reliable downloads for Alpine Linux users worldwide. The T1 mirrors require
   5TB of disk space each and currently use approximately 800TB of bandwidth
   per month.
2. *CI Runners*
   Two servers act as continuous integration (CI) runners for x86_64 and x86
   architectures. These runners are crucial for building and testing packages,
   ensuring Alpine Linux remains a robust and secure platform. For CI
   workloads, we require at least 50GB of disk space and a minimum of 16GB of
   RAM, though more resources are preferable to support building large packages
   like Chromium.
3. *Development Box*
   A single server has been dedicated as a development environment for
   contributors and maintainers, providing a performant workspace for
   day-to-day tasks and troubleshooting.

## How the Community Can Help

To continue providing Alpine Linux as a reliable, secure, and efficient
operating system, we need your help. Here are the specific areas where support
is most needed:

### 1. Colocation Space

We are seeking colocation space near the Netherlands (NLD) to allow for easier
server installation and maintenance. If you can provide or sponsor rack space
for our servers, please get in touch.

### 2. Bare-Metal Servers

Bare-metal servers are critical to replacing the infrastructure we are losing.
Specifically, we need:

- High-performance servers for our T1 mirroring infrastructure.
- Servers with sufficient compute power to handle CI workloads for x86_64
  and x86.

### 3. Virtual Machines

If bare-metal servers are not feasible, virtual machines with adequate storage,
compute, and network capabilities could serve as an alternative for our
development and CI environments.

## Supporting Alpine Linux Financially

This transition highlights the need for sustainable funding to maintain Alpine
Linux. It is [now possible to contribute financially](https://www.alpinelinux.org/posts/2025-01-30-alpine-linux-joins-open-collective.html) to Alpine Linux through
[Open Collective](https://opencollective.com/alpinelinux), a transparent
platform where donations directly support our infrastructure and development
needs.

We are incredibly grateful for the support of our community and sponsors. If
you or your organization can assist with hosting resources or financial
contributions, please don't hesitate to reach out. Together, we can ensure the
Alpine Linux project remains a robust and vital part of the Open Source
ecosystem.

## Contact Us

For inquiries about infrastructure support or financial contributions, please
email us at
[infra@alpinelinux.org](mailto:infra@alpinelinux.org).

Thank you for being part of the Alpine Linux community and supporting our journey forward!

