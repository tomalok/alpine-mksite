---
title: 'Alpine 3.15.11, 3.16.8, 3.17.6 and 3.18.5 released'
date: 2023-11-30
---

Alpine 3.15.11, 3.16.8, 3.17.6 and 3.18.5 released
===========================

The Alpine Linux project is pleased to announce the immediate
availability of new stable releases:

- [3.15.11](https://git.alpinelinux.org/aports/log/?h=v3.15.11)
- [3.16.8](https://git.alpinelinux.org/aports/log/?h=v3.16.8)
- [3.17.6](https://git.alpinelinux.org/aports/log/?h=v3.17.6)
- [3.18.5](https://git.alpinelinux.org/aports/log/?h=v3.18.5)

Those releases include security fixes for openssl:

- [CVE-2023-5678](https://security.alpinelinux.org/vuln/CVE-2023-5678)
- [CVE-2023-5363](https://security.alpinelinux.org/vuln/CVE-2023-5363)

