---
title: 'Alpine 3.15.10, 3.16.7, 3.17.5 and 3.18.3 released'
date: 2023-08-07
---

Alpine 3.15.10, 3.16.7, 3.17.5 and 3.18.3 released
===========================

The Alpine Linux project is pleased to announce the immediate
availability of new stable releases:

- [3.15.10](https://git.alpinelinux.org/aports/log/?h=v3.15.10)
- [3.16.7](https://git.alpinelinux.org/aports/log/?h=v3.16.7)
- [3.17.5](https://git.alpinelinux.org/aports/log/?h=v3.17.5)
- [3.18.3](https://git.alpinelinux.org/aports/log/?h=v3.18.3)

Those releases include security fixes for openssl:

- [CVE-2023-2975](https://security.alpinelinux.org/vuln/CVE-2023-2975)
- [CVE-2023-3446](https://security.alpinelinux.org/vuln/CVE-2023-3446)
- [CVE-2023-3817](https://security.alpinelinux.org/vuln/CVE-2023-3817)

