---
title: 'Alpine 3.10.8, 3.11.10 and 3.12.6 released'
date: 2021-03-31
---

Alpine 3.10.8, 3.11.10 and 3.12.6 released
===========================

The Alpine Linux project is pleased to announce the immediate
availability of version 3.10.8, 3.11.10 and 3.12.6 of its Alpine
Linux operating system.

Those releases include fixes for busybox [CVE-2021-28831](https://gitlab.alpinelinux.org/alpine/aports/-/issues/12566)


