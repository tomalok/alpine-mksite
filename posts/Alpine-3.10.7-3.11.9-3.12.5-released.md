---
title: 'Alpine 3.10.7, 3.11.9 and 3.12.5 released'
date: 2021-03-25
---

Alpine 3.10.7, 3.11.9 and 3.12.5 released
===========================

The Alpine Linux project is pleased to announce the immediate
availability of version 3.10.7, 3.11.9 and 3.12.5 of its Alpine
Linux operating system.

Those releases include an important security fixes for [openssl](https://www.openssl.org/news/secadv/20210325.txt):

- [CVE-2021-3449](https://cve.mitre.org/cgi-bin/cvename.cgi?name=2021-3449)
- [CVE-2021-3450](https://cve.mitre.org/cgi-bin/cvename.cgi?name=2021-3450)

