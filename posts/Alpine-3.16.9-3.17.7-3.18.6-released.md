---
title: 'Alpine 3.16.9, 3.17.7 and 3.18.6 released'
date: 2024-01-26
---

Alpine 3.16.9, 3.17.7 and 3.18.6 released
===========================

The Alpine Linux project is pleased to announce the immediate
availability of new stable releases:

- [3.16.9](https://git.alpinelinux.org/aports/log/?h=v3.16.9)
- [3.17.7](https://git.alpinelinux.org/aports/log/?h=v3.17.7)
- [3.18.6](https://git.alpinelinux.org/aports/log/?h=v3.18.6)

Those releases include security fixes for openssl:

- [CVE-2023-6129](https://security.alpinelinux.org/vuln/CVE-2023-6129)
- [CVE-2023-6237](https://security.alpinelinux.org/vuln/CVE-2023-6237)
- [CVE-2024-0727](https://security.alpinelinux.org/vuln/CVE-2024-0727)

