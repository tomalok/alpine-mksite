---
title: 'Alpine 3.18.12, 3.19.7, 3.20.6 and 3.21.3 released'
date: 2025-02-13
---

Alpine 3.18.12, 3.19.7, 3.20.6 and 3.21.3 released
===========================

The Alpine Linux project is pleased to announce the immediate
availability of new stable releases:

- [3.18.12](https://git.alpinelinux.org/aports/log/?h=v3.18.12)
- [3.19.7](https://git.alpinelinux.org/aports/log/?h=v3.19.7)
- [3.20.6](https://git.alpinelinux.org/aports/log/?h=v3.20.6)
- [3.21.3](https://git.alpinelinux.org/aports/log/?h=v3.21.3)

These releases include security fixes for:

- OpenSSL: [CVE-2024-13176](https://security.alpinelinux.org/vuln/CVE-2024-13176) and [CVE-2024-12797](https://security.alpinelinux.org/vuln/CVE-2024-12797)
- musl: [CVE-2025-26519](https://security.alpinelinux.org/vuln/CVE-2025-26519)


