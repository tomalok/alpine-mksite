---
title: 'Alpine 3.11.13, 3.12.9 and 3.13.7 released'
date: 2021-11-12
---

Alpine 3.11.13, 3.12.9 and 3.13.7 released
===========================

The Alpine Linux project is pleased to announce the immediate
availability of version [3.11.13](https://git.alpinelinux.org/aports/log/?h=v3.11.13),
[3.12.9](https://git.alpinelinux.org/aports/log/?h=v3.12.9) and
[3.13.7](https://git.alpinelinux.org/aports/log/?h=v3.13.7) of its Alpine
Linux operating system.

Those releases include various security fixes for busybox:

- [CVE-2021-42374](https://security.alpinelinux.org/vuln/CVE-2021-42374)
- [CVE-2021-42375](https://security.alpinelinux.org/vuln/CVE-2021-42375)
- [CVE-2021-42378](https://security.alpinelinux.org/vuln/CVE-2021-42378)
- [CVE-2021-42379](https://security.alpinelinux.org/vuln/CVE-2021-42379)
- [CVE-2021-42380](https://security.alpinelinux.org/vuln/CVE-2021-42380)
- [CVE-2021-42381](https://security.alpinelinux.org/vuln/CVE-2021-42381)
- [CVE-2021-42382](https://security.alpinelinux.org/vuln/CVE-2021-42382)
- [CVE-2021-42383](https://security.alpinelinux.org/vuln/CVE-2021-42383)
- [CVE-2021-42384](https://security.alpinelinux.org/vuln/CVE-2021-42384)
- [CVE-2021-42385](https://security.alpinelinux.org/vuln/CVE-2021-42385)
- [CVE-2021-42386](https://security.alpinelinux.org/vuln/CVE-2021-42386)

