---
title: 'Alpine 3.17.10, 3.18.9, 3.19.4, 3.20.3 released'
date: 2024-09-06
---

Alpine 3.17.10, 3.18.9, 3.19.4, 3.20.3 released
===========================

The Alpine Linux project is pleased to announce the immediate
availability of new stable releases:

- [3.17.10](https://git.alpinelinux.org/aports/log/?h=v3.17.10)
- [3.18.9](https://git.alpinelinux.org/aports/log/?h=v3.18.9)
- [3.19.4](https://git.alpinelinux.org/aports/log/?h=v3.19.4)
- [3.20.3](https://git.alpinelinux.org/aports/log/?h=v3.20.3)

Those releases contains various security fixes including a fix for OpenSSL
[CVE-2024-6119](https://security.alpinelinux.org/vuln/CVE-2024-6119).
