---
title: 'Alpine 3.13.11, 3.14.7 and 3.15.5 released'
date: 2022-07-19
---

Alpine 3.13.11, 3.14.7 and 3.15.5 released
===========================

The Alpine Linux project is pleased to announce the immediate
availability of new stable releases:

- [3.13.11](https://git.alpinelinux.org/aports/log/?h=v3.13.11)
- [3.14.7](https://git.alpinelinux.org/aports/log/?h=v3.14.7)
- [3.15.5](https://git.alpinelinux.org/aports/log/?h=v3.15.5)

Those releases fixes:

- busybox [CVE-2022-30065](https://security.alpinelinux.org/vuln/CVE-2022-28391)
- openssl [CVE-2022-2097](https://security.alpinelinux.org/vuln/CVE-2022-2097)

