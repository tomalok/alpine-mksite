---
title: 'Alpine 3.15.9, 3.16.6, 3.17.4 and 3.18.2 released'
date: 2023-06-14
---

Alpine 3.15.9, 3.16.6, 3.17.4 and 3.18.2 released
===========================

The Alpine Linux project is pleased to announce the immediate
availability of new stable releases:

- [3.15.9](https://git.alpinelinux.org/aports/log/?h=v3.15.9)
- [3.16.6](https://git.alpinelinux.org/aports/log/?h=v3.16.6)
- [3.17.4](https://git.alpinelinux.org/aports/log/?h=v3.17.4)
- [3.18.2](https://git.alpinelinux.org/aports/log/?h=v3.18.2)

Those releases include security fixes for openssl:

- [CVE-2023-1255](https://security.alpinelinux.org/vuln/CVE-2023-1255)
- [CVE-2023-2650](https://security.alpinelinux.org/vuln/CVE-2023-2650)

