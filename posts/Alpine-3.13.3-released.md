---
title: 'Alpine 3.13.3 released'
date: 2021-03-25
---

Alpine Linux 3.13.3 released
===========================

The Alpine Linux project is pleased to announce the immediate
availability of version 3.13.3 of its Alpine Linux operating system.

This release includes a security update for [openssl](https://www.openssl.org/news/secadv/20210325.txt) which fixes:

- [CVE-2021-3449](https://cve.mitre.org/cgi-bin/cvename.cgi?name=2021-3449)
- [CVE-2021-3450](https://cve.mitre.org/cgi-bin/cvename.cgi?name=2021-3450)

The full lists of changes can be found in the [git
log](http://git.alpinelinux.org/cgit/aports/log/?h=v3.13.3).

Git Shortlog
------------

<pre>
6543 (3):
      community/gitea: upgrade to 1.13.4
      community/gitea: upgrade to 1.13.5
      community/gitea: upgrade to 1.13.6

André Klitzing (1):
      community/flatpak: upgrade to 1.10.2

Andy Postnikov (9):
      main/nodejs: upgrade to 14.15.5
      community/php7-pecl-xdebug: upgrade to 3.0.3
      community/php8-pecl-xdebug: upgrade to 3.0.3
      main/postgresql: security upgrade to 13.2
      community/composer: upgrade to 2.0.10
      community/composer: upgrade to 2.0.11
      community/php7-pecl-ssh2: upgrade to 1.3.1
      community/php7-pecl-apcu: upgrade to 5.1.20
      community/php8-pecl-apcu: upgrade to 5.1.20

Ariadne Conill (1):
      main/nagios: fix truncation of pairlist in getcgivars(), closes #12516

Bart Ribbers (22):
      community/kde-release-service: upgrade to 20.12.2
      community/libquotient: upgrade to 0.6.5
      community/plasma-angelfish: upgrade to 1.8.0
      community/kclock: upgrade to 0.4.0
      community/kalk: upgrade to 0.2
      community/kweather: upgrade to 0.4
      community/mauikit: upgrade to 1.2.1
      community/buho: upgrade to 1.2.1
      community/vvave: upgrade to 1.2.1
      community/index: upgrade to 1.2.1
      community/nota: upgrade to 1.2.1
      community/pix: upgrade to 1.2.1
      community/boca: upgrade to 1.0.4
      community/freac: upgrade to 1.1.4
      community/libquotient: disable E2EE, it's broken
      community/okular: split qml files to it's own subpackage
      {community,testing}/kde-release-service: upgrade to 20.12.3
      community/gwenview: disable failing tests
      community/flatbuffers: re-enable on armv7 and modernize
      community/sink: re-enable on armv7
      community/openrct2: upgrade to 0.3.3
      community/pipewire: add .desktop file to /etc/xdg/autostart

Daniel Néri (3):
      community/isync: upgrade to 1.3.5
      main/xen: fix XSA-364/CVE-2021-26933
      main/xen: CVE-2021-3308 assigned to XSA-360

Dermot Bradley (1):
      [3.13] community/cloud-init: fix CVE-2021-3429

Duncan Bellamy (1):
      community/dovecot-fts-xapian: upgrade to 1.4.8

Francesco Colista (3):
      main/bacula: bacula-client should ship also bacula-client-openrc
      community/jenkins: security upgrade to 2.281.
      community/lua-luatz: new aport

Henrik Riomar (1):
      main/intel-ucode: security upgrade to 20210216

J0WI (5):
      community/firefox-esr: security upgrade to 78.8.0
      community/mozjs78: security upgrade to 78.8.0
      main/pgpool: upgrade to 4.2.2
      main/wpa_supplicant: patch CVE-2021-27803
      community/tor: security upgrade to 0.4.4.8

Jake Buchholz (1):
      community/containerd: security update to 1.4.4

Jakub Jirutka (4):
      main/nodejs: security upgrade to 14.16.0
      community/nodejs-current: security upgrade to 15.10.0
      community/ruby-ox: upgrade to 2.14.3
      community/log4cplus: upgrade to 2.0.6

Kaarle Ritvanen (2):
      community/py3-django-haystack: fix dependencies
      community/py3-django-oscar: fix translations

Kevin Daudt (6):
      community/vifm: add file to depends
      main/curl: backport fix for -w time_total output
      community/containerd: disable on mips(64)
      main/git: security upgrade to 2.30.2 (CVE-2021-21300)
      community/docker: add ip6tables dependency to docker-engine
      main/haserl: security upgrade to 0.9.36 (CVE-2021-29133)

Leo (23):
      community/libebml: security upgrade to 1.4.2
      community/libmatroska: upgrade to 1.6.3
      main/python3: fix CVE-2021-3177
      community/yelp-xsl: upgrade to 3.38.3
      community/ntpsec: fix location of library
      community/gexiv2: upgrade to 0.12.2
      main/libbsd: add missing secfixes info
      community/mumble: fix CVE-2021-27339
      community/py3-django: upgrade to 3.1.7
      main/openldap: fix CVE-2021-27212
      main/asterisk: security upgrade to 18.2.1
      community/ssh-audit: disable on mips64
      main/openjpeg: add misssing secfixes info
      main/glib: fix symlink attack
      main/gnutls: upgrade to 3.7.1
      main/gptfdisk: upgrade to 1.0.7
      community/wireshark: security upgrade to 3.4.4
      main/tiff: security upgrade to 4.2.0
      community/gtk4.0: upgrade to 4.0.3
      community/btrbk: upgrade to 0.31.2
      main/gdk-pixbuf: upgrade to 2.42.4
      community/rpm: upgrade to 4.16.1.3
      community/pipewire: upgrade to 0.3.24

Leonardo Arena (6):
      community/nextcloud: upgrade to 20.0.7
      community/nextcloud19: upgrade to 19.0.8
      community/nextcloud: allow replacing of v19
      community/nextcloud: upgrade to 20.0.8
      community/nextcloud19: upgrade to 19.0.9
      Revert "community/nextcloud: allow replacing of v19"

Michał Polański (7):
      community/wdisplays: fix segfault
      community/ssh-audit: upgrade to 2.4.0
      community/ginkgo: upgrade to 1.15.1
      community/hcloud: upgrade to 1.21.0
      community/go: upgrade to 1.15.10
      community/ginkgo: upgrade to 1.15.2
      community/hcloud: upgrade to 1.21.1

Milan P. Stanić (3):
      community/mutt: upgrade to 2.0.6
      main/haproxy: upgrade to 2.2.10
      main/haproxy: upgrade to 2.2.11

Natanael Copa (23):
      main/mdadm: fix use-after-free
      main/python3: security upgrade to 3.8.8 (CVE-2021-23336)
      main/linux-lts: upgrade to 5.10.24
      community/jool-modules-lts: rebuild against kernel 5.10.24-r0
      community/rtl8821ce-lts: rebuild against kernel 5.10.24-r0
      community/rtpengine-lts: rebuild against kernel 5.10.24-r0
      main/dahdi-linux-lts: rebuild against kernel 5.10.24-r0
      main/xtables-addons-lts: rebuild against kernel 5.10.24-r0
      main/zfs-lts: rebuild against kernel 5.10.24-r0
      main/openssh: fix CVE-2021-28041
      main/linux-lts: upgrade to 5.10.26
      community/jool-modules-lts: rebuild against kernel 5.10.26-r0
      community/rtl8821ce-lts: rebuild against kernel 5.10.26-r0
      community/rtpengine-lts: rebuild against kernel 5.10.26-r0
      main/dahdi-linux-lts: rebuild against kernel 5.10.26-r0
      main/xtables-addons-lts: rebuild against kernel 5.10.26-r0
      main/zfs-lts: rebuild against kernel 5.10.26-r0
      main/linux-rpi: upgrade to 5.10.26
      community/jool-modules-rpi: rebuild against kernel 5.10.26-r0
      main/zfs-rpi: rebuild against kernel 5.10.26-r0
      main/openssl: upgrade to 1.1.1k (CVE-2021-3449, CVE-2021-3450)
      main/raspberrypi-bootloader: upgrade to 1.20210303
      ===== release 3.13.3 =====

Stefan Krah (1):
      main/python3: add reliability fix for test_nntplib

Sören Tempel (1):
      community/go: fix test suite on Alpine CI

TBK (1):
      main/redis: security upgrade to 6.0.11

Thomas Liske (3):
      community/ifstate: upgrade to 1.5.0
      community/ifstate: upgrade to 1.5.1
      main/libseccomp: enable python bindings

omni (1):
      main/mbedtls: security upgrade to 2.16.10
</pre>
