---
title: 'Alpine Linux joins Open Collective'
date: 2025-01-30
---
# Alpine Linux joins Open Collective

We are pleased to announce that Alpine Linux has joined Open Collective, a platform that enables individuals and organizations to financially support open source projects and communities. This move is a significant step in ensuring the long-term sustainability of Alpine Linux, particularly in light of its growing popularity.

One of the primary motivations behind this decision is to reduce our reliance on a small number of sponsors. While we are immensely grateful for their support, we recognize the importance of diversifying our funding sources to mitigate potential risks. By joining Open Collective, we open up opportunities for a broader community of users and enthusiasts to contribute to the project's infrastructure.

Furthermore, we have received numerous requests from individuals and organizations expressing their desire to contribute financially to Alpine Linux. However, until now, there hasn't been a formal mechanism in place to facilitate such contributions. Open Collective provides a transparent and accountable platform that will allow us to accept donations and ensure that they are used solely for the benefit of the project.

Currently, all contributions made through Open Collective will be directed towards infrastructure hardware or services, such as servers, storage, and bandwidth. Individual developers and contributors will not receive any direct compensation from these contributions. While our current focus is on infrastructure, we may explore using funds to support developers and contributors in the future. We will notify the community if and when this funding policy changes.

We invite our community members to join us in supporting the project's continued growth and success. Together, we can ensure that Alpine Linux remains a vibrant and sustainable open source project for years to come.

To contribute, please visit our [Open Collective](https://opencollective.com/alpinelinux) page. Your support, no matter how small, will be appreciated.
