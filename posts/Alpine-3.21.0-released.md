---
title: 'Alpine 3.21.0 released'
date: 2024-12-05
---

Alpine Linux 3.21.0 Released
============================

We are pleased to announce the release of Alpine Linux 3.21.0, the first in
the v3.21 stable series.

<a name="highlights">Highlights</a>
----------

* Linux kernel [6.12](https://kernelnewbies.org/Linux_6.12)
* GCC [14](https://gcc.gnu.org/gcc-14/changes.html)
* LLVM [19](https://releases.llvm.org/19.1.0/docs/ReleaseNotes.html)
* Node.js (lts) [22.11](https://nodejs.org/en/blog/release/v22.11.0)
* Rust [1.83](https://blog.rust-lang.org/2024/11/28/Rust-1.83.0.html)
* Crystal [1.14](https://github.com/crystal-lang/crystal/releases/tag/1.14.0)
* GNOME [47](https://release.gnome.org/47/)
* Go [1.23](https://go.dev/blog/go1.23)
* KDE Plasma [6.2](https://kde.org/announcements/plasma/6/6.2.0/)
* LXQt [2.1](https://lxqt-project.org/release/2024/11/05/release-lxqt-2-1-0/)
* PHP [8.4](https://www.php.net/releases/8.4/en.php)
* Qt [6.8](https://www.qt.io/blog/qt-6.8-released)
* Sway [1.10](https://github.com/swaywm/sway/releases/tag/1.10)
* .NET [9.0](https://learn.microsoft.com/en-us/dotnet/core/whats-new/dotnet-9/overview)

<a name="significant_changes">Significant changes</a>
-------------------

Initial support for loongarch64 was added.


<a name="upgrade_notes">Upgrade notes</a>
-------------

As always, make sure to use `apk upgrade --available` when switching between
major versions.

OpenSSH service will [automatically restart](https://wiki.alpinelinux.org/wiki/Release_Notes_for_Alpine_3.21.0#OpenSSH_service_requires_restart)
when upgrading to be able to accept new connections.

The `linux-firmware` is now [compressed with
ZSTD](https://wiki.alpinelinux.org/wiki/Release_Notes_for_Alpine_3.21.0#linux-firmware).
Users running custom-built kernels need to ensure that
`CONFIG_FW_LOADER_COMPRESS_ZSTD=y` is present in the kernel configuration.

Users with `/` and `/usr` on separate filesystems (which is unsupported) need take special care. See the
[wiki for details](https://wiki.alpinelinux.org/wiki/Release_Notes_for_Alpine_3.21.0#Preparations_for_/usr-merge).

<a name="changes">Changes</a>
-------

The full list of changes can be found in the [wiki][1],  [git log][2] and [bug tracker][3].

<a name="credits">Credits</a>
-------

Thanks to everyone sending patches, bug reports, new and updated aports,
and to everyone helping with writing documentation, maintaining the
infrastructure, or contributing in any other way!

Thanks to [GIGABYTE][4], [Linode][5], [Fastly][6], [IBM][7], [Equinix Metal][8],
[vpsFree][9] and [AlpineLinuxSupport.com][10] for providing us with hardware and hosting.

[1]: https://wiki.alpinelinux.org/wiki/Release_Notes_for_Alpine_3.21.0
[2]: https://git.alpinelinux.org/cgit/aports/log/?h=v3.21.0
[3]: https://gitlab.alpinelinux.org/alpine/aports/issues?scope=all&utf8=%E2%9C%93&state=closed&milestone_title=3.21.0
[4]: https://www.gigabyte.com/
[5]: https://linode.com
[6]: https://www.fastly.com/
[7]: https://ibm.com/
[8]: https://www.equinix.com/
[9]: https://vpsfree.org
[10]: https://alpinelinuxsupport.com/

### aports Commit Contributors

<pre>

6543
Adam Jensen
Adam Thiede
Adrian Siekierka
Ahmet Ibrahim Aksoy
Aiden Grossman
Alan Pope
Alejandro Liu
Alejandro Ojeda Gutiérrez
Aleks Bunin
Alex
Alex Denes
Alex Klinkhamer
Alex McGrath
Alexander Sharov
Alexandre Mutel
Alexey Yerin
Alistair Francis
Amelia Clarke
Andrej Kolchin
Andrej Kolčin
Andres Almiray
André Klitzing
Andy Hawkins
Andy Postnikov
Angelo Verlain
Angelo Verlain Shema
Anjandev Momi
Anri Dellal
Antoine Martin
Antoni Aloy Torrens
Ariadne Conill
Arnav Singh
Aster Boese
Atsushi Watanabe
Axel Garcia K.
Bart Ribbers
Benjamin Chenebault
Billy Moses
Biswapriyo Nath
Bobby Hamblin
Bohdan
Brad House
Brandon Boese
Brooss Teambb
Bryce Vandegrift
Callum Andrew
Callum Brown
Carl Chave
Carlo Landmeter
Carter Li
Celeste
Chleba
Christopher Angelo Phillips
Christopher Davis
Clayton Craft
Coco Liliace
Conrad Hoffmann
Consus
Cowington Post
Craig Andrews
D Singh
DWwanghao
DaKnig
Daniel Fancsali
Daniel Hejduk
Daniel Mizyrycki
Daniel Néri
Dave Henderson
David Demelier
David Florness
David Heidelberg
David Sugar
Dawid Dziurla
Dekedro
Devin Lin
Dhruvin Gandhi
Dmitry Davydov
Dominique Martinet
Duncan Bellamy
Dylan Van Assche
Díaz Urbaneja Víctor Diego Alejandro (Sodomon)
Edd Salkield
Elly Fong-Jones
Fabricio Silva
Faustin Lammler
Federico
Felix Singer
FintasticMan
Fiona Klute
Firas Khalil Khana
Florian Uhlich
Forza
Francesco Colista
Frank Oltmanns
Galen Abell
Gil Pedersen
GitHub Action
Guillaume Quintard
Guy Godfroy
Haelwenn (lanodan) Monnier
Hannes Braun
Henrik Grimler
Henrik Riomar
Hoang Nguyen
Holger Jaekel
Hristiyan Ivanov
Hugo Osvaldo Barrera
Hugo Rodrigues
Hugo Wang
IWAI, Masaharu
Ian Dunbar-Hall
Ievgen Pyrogov
Igor
Iskren Chernev
Iztok Fister Jr.
Iztok Fister, Jr
J0WI
Jake Buchholz Göktürk
Jake Smith
Jakob Hauser
Jakob Meier
Jakub Jirutka
Jason Swank
Jeff Dickey
Jens Reidel
Jesse Mandel
Jian-Hong Pan
Jingyun Hua
Jo Zzsi
Joel Hansen
Joel Selvaraj
Johannes Heimansberg
Johannes Marbach
John
John Anthony
John Nunley
John Vogel
Jonas
Jonas Fierlings
Jonathan Schleifer
Jordan Christiansen
Josef Vybíhal
Joseph Benden
Joshua Murphy
Kaarle Ritvanen
Kaspar Schleiser
Kasper K
Kate
Kevin Daudt
Kleis Auke Wolthuizen
Klemens Nanni
Konstantin Kulikov
Krassy Boykinov
Krystian Chachuła
L. E. Segovia
LN Liberda
Lassebq
Laurent Bercot
Lauri Tirkkonen
Leon Marz
Leon White
Leonardo Arena
Liam
Lindsay Zhou
Linux User
Luca Weiss
Lucas Larson
Lucidiot
M Hickford
Maarten van Gompel
Magnus Sandin
Mai Thanh Minh
Marco Schröder
Maria Lisina
Marian Buschsieweke
Mathias Lang
Mathieu Mirmont
Matt Holden
Matthias Ahouansou
Matthias Valvekens
Maxim Karasev
Mek Yt
Meng Zhuo
Michael M
Michael Pirogov
Michal Tvrznik
Michał Adamski
Michał Polański
Micheal Smith
Mike Crute
Milan P. Stanić
Miles Alan
Mogens Jensen
Mohammad AlSaleh
NN708
Natanael Copa
Newbyte
Nico Schottelius
Nicolas Lorin
Niklas Meyer
Noel Kuntze
Oleg Titov
Oliver Smith
Olivier Mauras
Orgad Shaneh
Orhun Parmaksız
Pablo Correa Gómez
Paolo Barbolini
Patrick Gansterer
Peter Mack
Peter Shkenev
Peter van Dijk
Petr Vorel
Phil Estes
Piraty
Rabindra Dhakal
Rafael Ávila de Espíndola
Rane Hebden
Ricardo Fraile
Richard Acayan
Rikj000
Robert Mader
Robin Candau
Rosie K Languet
Rudolf Polzer
Runxi Yu
Ruven
Ryan Walklin
Sadie Powell
Saijin-Naib
Sam Day
Sam Nystrom
Santurysim
Sean E. Russell
Sean McAvoy
Sergiy Stupar
Sertonix
Sicelo A. Mhlongo
Siebe Claes
Sijawusz Pur Rahnama
Simon Frankenberger
Simon Rupf
Simon Zeni
Siva Mahadevan
Stefan Hansson
Steffen Nurpmeso
Stephan Jauernick
Stephen
Stone Tickle
Síle Ekaterin Liszka
Sören Tempel
Ted Trask
Teemu Ikonen
The one with the braid
Thiago Perrotta
Thijs Iris Wester
Thomas Adam
Thomas Aldrian
Thomas Böhler
Thomas Deutsch
Thomas J Faughnan Jr
Thomas Kienlen
Thomas Liske
Tom Wieczorek
Tomoya Tanjo
Tuan Anh Tran
Tyler Amick
Wang Qi
Weijie Wang
Wenlong Zhang
Will Sinatra
Willow Barraco
Wojtek Kruszewski
Wolfgang Fischer
Z
Zach DeCook
Zachary Lee Andrews
Zoey
adamthiede
amolinae06
blacksilver
cos
crapStone
dmitz
doitwithnotepad
donoban
duckl1ng
fossdd
hash
jahway603
jane400
jo
jvoisin
knuxify
kpcyrd
lauren n. liberda
leso-kn
macmpi
mekyt
mini-bomba
mio
mrhh69
nibon7
odi79
ogromny
omni
ovf
oxpa
prspkt
psykose
ptrcnull
qaqland
qiangxuhui
rdbo
rickyrockrat
rubicon
snowdream
socksinspace
sodface
stepech
steve
streaksu
strophy
techknowlogick
tetsumaki
uselpa
wanghao
wener
wesley van tilburg
xrs
xtex
yzewei
zhaixiaojuan
znley
Ángel Castañeda
Štěpán Pechman
Чарльз Миллер
李通洲
</pre>
